import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput, Dimensions, Image, TouchableHighlight } from 'react-native';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../config/firebase';

export default function Signup({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const onHandleSignup = () => {
    if (email !== '' && password !== '') {
  createUserWithEmailAndPassword(auth, email, password)
        .then(() => console.log('Signup success'))
        .catch(err => console.log(`Login err: ${err}`));
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Create new account</Text>
      <View style={styles.imageContainer}>
        <Image
            style ={styles.image}
            source={require('../assets/massira-logo.png')}
        />
      </View>
      <TextInput
        style={styles.input}
        placeholder='Enter email'
        autoCapitalize='none'
        keyboardType='email-address'
        textContentType='emailAddress'
        value={email}
        onChangeText={text => setEmail(text)}
      />
      <TextInput
        style={styles.input}
        placeholder='Enter password'
        autoCapitalize='none'
        autoCorrect={false}
        secureTextEntry={true}
        textContentType='password'
        value={password}
        onChangeText={text => setPassword(text)}
      />
      <View style={styles.imageContainer}>
        <TouchableHighlight onPress={onHandleSignup}>
            <View style={styles.button1}>
            <Text style={styles.btnText}>Signup</Text>
            </View>
        </TouchableHighlight>
      </View>
      <View style={styles.imageContainer}>
        <TouchableHighlight onPress={() => navigation.navigate('Login')}>
            <View style={styles.button2}>
            <Text style={styles.btnText}>Go to Login</Text>
            </View>
        </TouchableHighlight>
      </View>
     
    
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 12
  },
  image : {
    width: 150,
    height: 70,
    alignItems: 'center'
    
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    color: '#444',
    alignSelf: 'center',
    paddingBottom: 24
  },
  input: {
    backgroundColor: '#fff',
    marginBottom: 20,
    fontSize: 16,
    borderWidth: 1,
    borderColor: '#333',
    borderRadius: 8,
    padding: 12
  },
  imageContainer : {
    alignItems:'center',
    marginBottom: 10
    },
  button1: {
    alignItems: "center",
    backgroundColor: "#f43566",
    borderRadius:15,
    marginBottom: 10,
    width : Dimensions.get('window').width/2,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    color : '#FFFFFF'
  },
  button2: {
    alignItems: "center",
    backgroundColor: "#4d0aa4",
    borderRadius:15,
    marginBottom: 10,
    width : Dimensions.get('window').width/2,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    color : '#FFFFFF'
  },
  btnText : {
      color: '#FFFFFF',
      fontSize: 20,
      fontWeight: 'bold'
  }
});