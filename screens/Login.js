import React, { useState } from 'react';
import { StyleSheet, Text, View, Button, TextInput,Image, ViewBase, ViewComponent,TouchableHighlight, Dimensions } from 'react-native';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../config/firebase';
import { requireNativeViewManager } from 'expo-modules-core';

export default function Login({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  const windowWidth = Dimensions.get('window').width;
  const windowHeight = Dimensions.get('window').height;
  const onHandleLogin = () => {
    if (email !== '' && password !== '') {
     signInWithEmailAndPassword(auth, email, password)
        .then(() => console.log('Login success'))
        .catch(err => console.log(`Login err: ${err}`));
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Welcome back!!</Text>
      <View style={styles.imageContainer}>
      <Image
        style ={styles.image}
        source={require('../assets/massira-logo.png')}
      />
      </View>
     
      <TextInput
        style={styles.input}
        placeholder='Enter email'
        autoCapitalize='none'
        keyboardType='email-address'
        textContentType='emailAddress'
        autoFocus={true}
        value={email}
        onChangeText={text => setEmail(text)}
      />
      <TextInput
        style={styles.input}
        placeholder='Enter password'
        autoCapitalize='none'
        autoCorrect={false}
        secureTextEntry={true}
        textContentType='password'
        value={password}
        onChangeText={text => setPassword(text)}
      />
      <View style={styles.imageContainer}>
        <TouchableHighlight onPress={onHandleLogin}>
            <View style={styles.button1}>
            <Text style={styles.btnText}>Login</Text>
            </View>
        </TouchableHighlight>
      </View>
      <View style={styles.imageContainer}>
        <TouchableHighlight onPress={() => navigation.navigate('Signup')}>
            <View style={styles.button2}>
            <Text style={styles.btnText}>Go to Signup</Text>
            </View>
        </TouchableHighlight>
      </View>
     
      
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    
    justifyContent: 'center',
    paddingHorizontal: 12
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    color: '#444',
    alignSelf: 'center',
    paddingBottom: 24
  },
  input: {
    backgroundColor: '#fff',
    marginBottom: 20,
    fontSize: 16,
    borderWidth: 1,
    borderColor: '#333',
    borderRadius: 8,
    padding: 12
  },
  image : {
    width: 150,
    height: 70,
    alignItems: 'center'
    
  },
  imageContainer : {
      alignItems:'center',
      marginBottom: 10
  },
  button1: {
    alignItems: "center",
    backgroundColor: "#f43566",
    borderRadius:15,
    marginBottom: 10,
    width : Dimensions.get('window').width/2,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    color : '#FFFFFF'
  },
  button2: {
    alignItems: "center",
    backgroundColor: "#4d0aa4",
    borderRadius:15,
    marginBottom: 10,
    width : Dimensions.get('window').width/2,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11,
    color : '#FFFFFF'
  },
  btnText : {
      color: '#FFFFFF',
      fontSize: 20,
      fontWeight: 'bold'
  }
});